#!/usr/bin/ruby
class WarnSplit
  include Enumerable
  def initialize log
    @log = log
  end
  def each
    msg = ""
    seen_infu = false
    seen_warn = false
    @log.each_line{|l|
      case l
      when /modversion changed/
        #trash
      when /^In file included from/
        yield msg if msg.length > 0
        msg = l
        seen_infu = false
        seen_warn = false
      when /: In function /
        if seen_infu or seen_warn then
          yield msg
          msg = l
          seen_warn = false
        else
          msg << l
        end
        seen_infu = true
      when /:[0-9]+:[0-9]+: note:/
        msg << l
      when /:[0-9]+:([0-9]+:)? ?warning: /
        if seen_warn then
          yield msg
          seen_infu = false
          msg = l
        else
          msg << l
        end
        seen_warn = true
      else
        msg << l if l =~ /:[0-9]+:[0-9]+: / #junk lines that don't have line number or any other significant info
      end
    }
    yield msg if msg.length > 0
  end
end

class WarnParse
  attr_accessor :file, :origin, :reason, :text, :object, :function
  def hash
    file.hash | reason.hash | object.hash | function.hash
  end
  def == other
    (other.file == file) && (other.reason == reason) && (other.object == object) && (other.function == function)
  end
  def eql? other
    other.class == self.class && self == other
  end
  def initialize warning
    @text = warning
    @text.lines.each {|l|
      case l
      when /: In function /
        @file = l.sub(/:.*/,'').chomp
        @function = (l.match /: In function ‘([^‘’]+)’/)[1] rescue (l.match /: In function '([^']+)'/)[1] rescue nil
      when /:[0-9]+:([0-9]+:)? ?warning: /
        file = l.sub(/:.*/,'').chomp
        if @file and @file != file
          @origin = file
        else
          @file = file
        end
        @reason = (l.match /\[-W([^\]]+)\]/)[1] rescue (l.match /warning: (.+)/)[1]
        @object = (l.match /‘([^‘’]+)’/)[1] rescue (l.match /'([^']+)'/)[1] rescue nil
      end
    }
  end
  def short_s
    s = "File: '#@file'"
    s << " Origin: '#@origin'" if @origin
    s << " Reason: '#@reason'" if @reason
    s << " Object: '#@object'" if @reason
    s << "\n"
  end
  def to_s
    short_s << @text
  end
end

class WarnSort
  def initialize log=''
    @hash = Hash.new {|hsh, key| hsh[key] = [] }
    WarnSplit.new(log).each{|w|
      w=WarnParse.new(w)
      @hash[w.file] << w
    }
  end
  def to_s short=false
    return '' unless self.length > 0
    s="***** #{self.length} warnings *****\n"
    @hash.keys.each{|k|
      #s << "*** file: " << k << " ***\n"
      @hash[k].each{|w|
        #s << "* " << w.reason if w.reason
        #s << " (" << w.object << ")" if w.object
        #s << " in " << w.function if w.function
        #s << " (from " << w.origin << ")" if w.origin
        #s << "\n" if w.origin or w.reason
        s << w.text if ! short
      }
    }
    s
  end
  def keys
    @hash.keys
  end
  def [](key)
    @hash[key]
  end
  def diff old
    res = WarnSort.new
    keys.each{|k|
      ws = self[k] - old[k]
      res[k].concat ws if ws.length > 0
    }
    res
  end
  def length
    res = 0
    keys.each{|k|
      res += self[k].length
    }
    res
  end
end

def printhelp error=0
STDOUT << "#{$0} oldlog newlog\n#{$0} --test\n"
exit error
end

@sampledata = "include/config/auto.conf:7381:warning: symbol value '/etc/keys/x509_ima.der' invalid for IMA_X509_PATH
include/config/auto.conf:7825:warning: symbol value 'm' invalid for BRIDGE_NETFILTER
include/config/auto.conf:8206:warning: symbol value 'm' invalid for SPI_FSL_SPI
include/config/auto.conf:8518:warning: symbol value 'm' invalid for DEVFREQ_GOV_USERSPACE
include/config/auto.conf:8757:warning: symbol value 'm' invalid for SND_HDA_CODEC_ANALOG
include/config/auto.conf:8819:warning: symbol value 'm' invalid for SND_HDA_CODEC_REALTEK
"+'In file included from drivers/ata/libata-core.c:65:0:
drivers/ata/libata-core.c: In function ‘ata_dev_configure’:
include/linux/libata.h:1318:16: warning: ‘native_sectors’ may be used uninitialized in this function [-Wmaybe-uninitialized]
  ata_dev_printk(dev, KERN_INFO, fmt, ##__VA_ARGS__)
                ^
drivers/ata/libata-core.c:1324:6: note: ‘native_sectors’ was declared here
  u64 native_sectors;
      ^
drivers/atm/ambassador.c: In function ‘ucode_init’:
drivers/atm/ambassador.c:1971:19: warning: ‘fw’ may be used uninitialized in this function [-Wmaybe-uninitialized]
   release_firmware(fw);
                   ^
sound/drivers/serial-u16550.c: In function ‘snd_serial_probe’:
sound/drivers/serial-u16550.c:966:9: warning: ‘uart’ may be used uninitialized in this function [-Wmaybe-uninitialized]
  sprintf(card->longname, "%s [%s] at %#lx, irq %d",
         ^
In file included from include/linux/cache.h:4:0,
                 from include/linux/time.h:7,
                 from include/linux/stat.h:60,
                 from include/linux/module.h:10,
                 from drivers/block/floppy.c:167:
drivers/block/floppy.c: In function ‘fd_locked_ioctl’:
include/linux/kernel.h:625:26: warning: ‘size’ may be used uninitialized in this function [-Wmaybe-uninitialized]
  __min1 < __min2 ? __min1: __min2; })
                          ^
drivers/block/floppy.c:3379:6: note: ‘size’ was declared here
  int size;
      ^
sound/pci/atiixp_modem.c: In function ‘snd_atiixp_probe’:
sound/pci/atiixp_modem.c:299:20: warning: ‘chip2’ may be used uninitialized in this function [-Wmaybe-uninitialized]
  writel(value, chip2->remap_addr + ATI_REG_##reg)
                    ^
sound/pci/atiixp_modem.c:1290:23: note: ‘chip2’ was declared here
  struct atiixp_modem *chip2;
                       ^
sound/pci/atiixp_modem.c: In function ‘snd_atiixp_probe’:
sound/pci/atiixp_modem.c:299:20: warning: ‘chip’ may be used uninitialized in this function [-Wnonsense]
  writel(value, chip->remap_addr + ATI_REG_##reg)
                    ^
sound/pci/atiixp_modem.c:1290:23: note: ‘chip’ was declared here
  struct atiixp_modem *chip;
                       ^
sound/pci/atiixp.c: In function ‘snd_atiixp_probe’:
sound/pci/atiixp.c:1685:14: warning: ‘chip’ may be used uninitialized in this function [-Wmaybe-uninitialized]
    chip->ac97[0] ? snd_ac97_get_short_name(chip->ac97[0]) : "?",
              ^
'

@sampledata2 = "include/config/auto.conf:7381:warning: symbol value '/etc/keys/x509_ima.der' invalid for IMA_X509_PATH
include/config/auto.conf:7825:warning: symbol value 'm' invalid for BRIDGE_NETFILTER
include/config/auto.conf:8206:warning: symbol value 'm' invalid for SPI_FSL_SPI
include/config/auto.conf:8518:warning: symbol value 'm' invalid for DEVFREQ_GOV_FANTASY
include/config/auto.conf:8757:warning: symbol value 'm' invalid for SND_HDA_CODEC_ANALOG
include/config/auto.conf:8819:warning: symbol value 'm' invalid for SND_HDA_CODEC_REALTEK
"+'In file included from drivers/ata/libata-core.c:65:0:
drivers/ata/libata-core.c: In function ‘ata_dev_configure’:
include/linux/libata.h:1318:16: warning: ‘native_sectors’ may be used uninitialized in this function [-Wmaybe-uninitialized]
  ata_dev_printk(dev, KERN_INFO, fmt, ##__VA_ARGS__)
                ^
drivers/ata/libata-core.c:1324:6: note: ‘native_sectors’ was declared here
  u64 native_sectors;
      ^
drivers/atm/ambassador.c: In function ‘ucode_init’:
drivers/atm/ambassador.c:1971:19: warning: ‘fw’ may be used uninitialized in this function [-Wmaybe-uninitialized]
   release_firmware(fw);
                   ^
sound/drivers/serial-u16550.c: In function ‘snd_serial_probe’:
sound/drivers/serial-u16550.c:966:9: warning: ‘uart’ may be used uninitialized in this function [-Wmaybe-uninitialized]
  sprintf(card->longname, "%s [%s] at %#lx, irq %d",
         ^
In file included from include/linux/cache.h:4:0,
                 from include/linux/time.h:7,
                 from include/linux/stat.h:60,
                 from include/linux/module.h:10,
                 from drivers/block/floppy.c:167:
sound/pci/atiixp_modem.c: In function ‘snd_atiixp_probe’:
sound/pci/atiixp_modem.c:299:20: warning: ‘chip2’ may be used uninitialized in this function [-Wmaybe-uninitialized]
  writel(value, chip2->remap_addr + ATI_REG_##reg)
                    ^
sound/pci/atiixp_modem.c:1290:23: note: ‘chip2’ was declared here
  struct atiixp_modem *chip2;
                       ^
sound/pci/atiixp_modem.c: In function ‘snd_atiixp_probe’:
sound/pci/atiixp_modem.c:299:20: warning: ‘chip3’ may be used uninitialized in this function [-Wnonsense]
  writel(value, chip3->remap_addr + ATI_REG_##reg)
                    ^
sound/pci/atiixp_modem.c:1290:23: note: ‘chip’ was declared here
  struct atiixp_modem *chip;
                       ^
sound/pci/atiixp.c: In function ‘snd_atiixp_probe’:
sound/pci/atiixp.c:1685:14: warning: ‘chip’ may be used uninitialized in this function [-Wmaybe-uninitialized]
    chip->ac97[0] ? snd_ac97_get_short_name(chip->ac97[0]) : "?",
              ^
sound/pci/atiixp.c: In function ‘snd_atiixp_probe’:
sound/pci/atiixp.c:1685:14: warning: ‘chip2’ may be used uninitialized in this function [-Wmaybe-uninitialized]
    chip2->ac97[0] ? snd_ac97_get_short_name(chip2->ac97[0]) : "?",
              ^
'

class AssertionError < RuntimeError
end

def assert &block
  raise AssertionError unless yield
end

def diffstrings s1, s2
  require 'tempfile'
  dta1 = Tempfile.new(File.basename $0)
  dta1.write s1
  dta1.close
  dta2 = Tempfile.new(File.basename $0)
  dta2.write s2
  dta2.close
  diff = `diff -u #{dta1.path} #{dta2.path}`
  dta1.unlink
  dta2.unlink
  diff
end

def runtest
#STDOUT << WarnSplit.new(@sampledata).map{|w| WarnParse.new(w)}.to_a.join("\n-----------------------------------------------\n")
t =  WarnSort.new(@sampledata)

assert { t['include/config/auto.conf'].length == 6 }
assert { t['sound/pci/atiixp.c'].length == 1 }
assert { t['sound/pci/atiixp_modem.c'].length == 2 }
assert { t['drivers/block/floppy.c'].length == 1 }
assert { t['sound/drivers/serial-u16550.c'].length == 1 }
assert { t['drivers/atm/ambassador.c'].length == 1 }
assert { t['drivers/ata/libata-core.c'].length == 1 }

assert { t['sound/pci/atiixp_modem.c'][1].reason == 'nonsense' }
assert { t['sound/pci/atiixp_modem.c'][0].object == 'chip2' }
assert { t['sound/pci/atiixp_modem.c'][1].object == 'chip' }
assert { t['sound/pci/atiixp_modem.c'][0].function == 'snd_atiixp_probe' }
assert { t['drivers/ata/libata-core.c'][0].function == 'ata_dev_configure' }
assert { t['drivers/block/floppy.c'][0].function == 'fd_locked_ioctl' }

t.keys.each{|k|
  case k
  when 'drivers/block/floppy.c'
    assert { t[k][0].origin == 'include/linux/kernel.h' }
  when 'drivers/ata/libata-core.c'
    assert { t[k][0].origin == 'include/linux/libata.h' }
  else
    t[k].each{|w| assert { ! w.origin } }
  end
}

t.keys.each{|k|
  if t[k].length == 6 then
    assert{ t[k][0].reason =~ /symbol value/ }
  else
    assert{ t[k][0].reason == 'maybe-uninitialized' }
  end
}

STDOUT << (diffstrings @sampledata, @sampledata2)
STDOUT << "\n-----------------------------------------------\n"
t2 =  WarnSort.new(@sampledata2)
STDOUT << (t2.diff t).to_s(true)
STDOUT << "\n-----------------------------------------------\n"
STDOUT << (t2.diff t).to_s

exit 0
end

if ARGV.length == 1 then
  runtest if ARGV[0] == '--test'
  printhelp if ARGV[0] == '--help'
end

printhelp(-1) if ARGV.length != 2

oldfile = File.open(ARGV[0])
newfile = File.open(ARGV[1])
old = WarnSort.new oldfile
new = WarnSort.new newfile
diff = new.diff old
STDOUT << diff.to_s
#STDERR << (diffstrings old.to_s, new.to_s)
exit diff.length if diff.length < 100
exit 100
